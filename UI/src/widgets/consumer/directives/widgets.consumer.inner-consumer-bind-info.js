(function (window, angular) {
    "use strict";

    angular.module('module.widgets.consumer')
        .directive('innerConsumerBindInfo', [
            function () {
                return {
                    restrict: 'E',
                    templateUrl: 'widgets.consumer.consumer-bind-info.html',
                    replace: true
                };
            }]);

})(window, window.angular);