(function (window, angular) {
    "use strict";

    angular.module('module.widgets.consumer')
        .directive('innerConsumerBindDone', [
            function () {
                return {
                    restrict: 'E',
                    templateUrl: 'widgets.consumer.consumer-bind-done.html',
                    replace: true
                };
            }]);

})(window, window.angular);