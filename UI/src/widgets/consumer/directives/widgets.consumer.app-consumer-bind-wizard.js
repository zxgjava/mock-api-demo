(function (window, angular) {
    "use strict";

    angular.module('module.widgets.consumer')
        .directive('appConsumerBindWizard', [
            function () {
                return {
                    restrict: 'E',
                    scope: {
                        onGoBack: '&'
                    },
                    templateUrl: 'widgets.consumer.consumer-bind.html',
                    controller: 'widgets.consumer.ConsumerBindController',
                    replace: true
                };
            }]);

})(window, window.angular);