(function (window, angular) {
    "use strict";

    angular.module('module.widgets.consumer')
        .directive('innerConsumerBindCode', [
            function () {
                return {
                    restrict: 'E',
                    templateUrl: 'widgets.consumer.consumer-bind-code.html',
                    replace: true
                };
            }]);

})(window, window.angular);