(function (window, angular) {
  "use strict";

  angular.module('module.widgets.consumer', [
      'module.common'
  ]);

})(window, window.angular);