(function (window, angular) {
    "use strict";

    angular.module('module.widgets.consumer')
        .controller('widgets.consumer.ConsumerBindController', [
            '$scope',
            'consumerService',
            function ($scope, consumerService) {
                $scope.newConsumer = {};
                $scope.activeStep = "info";
                $scope.invalidMessage = {};

                var validatePwd = function (){
                    var isValid = true;
                    $scope.invalidMessage.pwd = null;
                    if ($scope.newConsumer.pwd1 && $scope.newConsumer.pwd2
                        && $scope.newConsumer.pwd1 !== $scope.newConsumer.pwd2) {
                        $scope.invalidMessage.pwd = '第二次输入的密码与第一次不一致！';
                        isValid = false;
                    }
                    return isValid;
                };

                $scope.$watch('newConsumer.pwd1', function () {
                    validatePwd();
                });

                $scope.$watch('newConsumer.pwd2', function () {
                    validatePwd();
                });

                $scope.$watch('newConsumer.name', function () {
                    $scope.invalidMessage.$infoForm = null;
                });

                $scope.$watch('newConsumer.code', function () {
                    $scope.invalidMessage.$codeForm = null;
                });

                $scope.gotoStep = function(step) {
                    $scope.activeStep = step;
                };

                $scope.submitInitLogin = function() {
//                    consumerService.initLogin({
//                        name: $scope.newConsumer.name,
//                        password: $scope.newConsumer.initpwd
//                    }).then(function(resp){
//                        if (resp.result === -1) {
//                            $scope.invalidMessage.$infoForm = '初始登录验证失败！';
//                        } else {
//                            $scope.gotoStep('code');
//                        }
//                    });
                	$scope.gotoStep('code');
                };

                $scope.submitFinishBind = function() {
                	if (!validatePwd()) {
                        return;
                    }
//                    consumerService.finishBind($scope.newConsumer).then(function(resp){
//                        if (resp && 'Active' === resp.status) {
//                            $scope.newConsumer = {};
//                            $scope.gotoStep('done');
//                        } else {
//                            $scope.invalidMessage.$codeForm = '无效的验证码！';
//                        }
//                    });
                	$scope.gotoStep('done');
                };
            }
        ]);

})(window, window.angular);