(function (window, angular) {
  "use strict";

  angular.module('module.recharge')
    .controller('recharge.MainController', [
          '$scope',
          '$state',
          '$stateParams',
          'mockmpService',
          'alertService',
          function ($scope, $state, $stateParams, mockmpService, alertService){
        	  $scope.code = $stateParams.code;
        	  $scope.state = $stateParams.state;
        	  $scope.userInfo = {};
        	  
        	  $scope.refreshData = function(){
        		  mockmpService.getConsumerRechargeRecords($scope.userInfo.openid).then(function (cResp) {
        			   $scope.rechargeList = cResp;
            	});
            };
        	  
        	  if ($scope.code) {
        		  //wxuser
        		  mockmpService.getConsumerWxUserInfo($scope.code).then(function (resp) {
            		  $scope.userInfo = resp;
            		  if ($scope.userInfo.openid) {
            			  $scope.refreshData();
            		  }
            	});
        	  }
        	  
        	  
              
              
    }]);

})(window, window.angular);