(function (window, angular) {
    "use strict";

    angular.module('module.common')
        .factory('mockmpService', [
            'Restangular',
            function (Restangular) {
                return {
                	getConsumerWxUserInfo: function (code) {
                		return Restangular.all('mockmp').one('wxuser', code).get();
                    },
                    
                    getConsumerMoneyInfo: function (openId) {
                    	return Restangular.all('mockmp').customGET('money', {'openId':openId});
                    },
                    
                    getConsumerRechargeRecords: function (openId) {
                		return Restangular.all('mockmp').all('recharge').getList({'openId': openId});
                    }
                };
            }]);

})(window, window.angular);