(function (window, angular) {
    "use strict";

    angular.module('module.config', [])
        .constant('appConfig', {
            rootContext: '/',

            stateName: {
                home: 'home',
                pay: 'pay',
                cost: 'cost',
                recharge: 'recharge',
                consumer: 'consumer',
                qrcode: 'qrcode',
                consumerBind: 'consumer-bind'
            },

            userStatus: {
                new: 'New',
                active: 'Active',
                inactive: 'Inactive'
            },
            
            qrcodeInitUrl: {
            	wechat: 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=',
            	alipay: ''
            }

        }).run([
            '$rootScope',
            'appConfig',
            function ($rootScope, appConfig) {
                $rootScope.appConfig = appConfig;
        }]);

})(window, window.angular);
