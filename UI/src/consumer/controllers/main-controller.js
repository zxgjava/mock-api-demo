(function (window, angular) {
  "use strict";

  angular.module('module.consumer')
    .controller('consumer.MainController', [
          '$scope',
          '$state',
          '$stateParams',
          'mockmpService',
          'alertService',
          function ($scope, $state, $stateParams, mockmpService, alertService){
        	  $scope.code = $stateParams.code;
        	  $scope.state = $stateParams.state;
        	  $scope.userInfo = {};
        	  $scope.moneyInfo = {'totalMoneyAmount':0};
        	  
        	  
        	  if ($scope.code) {
        		  //wxuser
        		  mockmpService.getConsumerWxUserInfo($scope.code).then(function (resp) {
            		  $scope.userInfo = resp;
            		  if ($scope.userInfo.openid) {
            			  //money
                		  mockmpService.getConsumerMoneyInfo($scope.userInfo.openid).then(function (mResp) {
                    		  $scope.moneyInfo = mResp;
                    	  });
            		  }
            	  });
        	  };
    }]);

})(window, window.angular);