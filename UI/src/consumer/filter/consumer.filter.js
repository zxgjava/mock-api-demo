(function(window, angular) {
	"use strict";

	
	angular.module('module.consumer').filter('formateTime', [ function() {
		return function(input){
			if (input.length == 14) {
				var time = new moment(input, "YYYYMMDDHHmmss");
				return moment(time).format("YYYY-MM-DD HH:mm");
			} else {
				return input;
			}
		}
	} ]);
	
	

})(window, window.angular);