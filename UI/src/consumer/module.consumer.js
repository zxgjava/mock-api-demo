(function (window, angular) {
  "use strict";

  angular.module('module.consumer', ['module.common', 'module.widgets.consumer']);

})(window, window.angular);