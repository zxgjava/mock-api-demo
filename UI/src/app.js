(function (window, angular) {
    "use strict";

    angular.module('module.main', [
        //lib
        'ui.bootstrap',
        'ui.router',
        'restangular',
        'angular-growl',
        'angularSpinner',

        //template
        'module.templates',
        //app
        'module.config',
        'module.common',
        'module.widgets',
        'module.home',
        'module.recharge',
        'module.consumer'

    ]).config([
        '$stateProvider',
        '$locationProvider',
        '$urlRouterProvider',
        'RestangularProvider',
        'growlProvider',
        'appConfig',
        function ($stateProvider, $locationProvider, $urlRouterProvider,
                  RestangularProvider, growlProvider, appConfig) {

        	$locationProvider.hashPrefix('!').html5Mode(false);

            $urlRouterProvider.otherwise("home");


            $stateProvider.state(appConfig.stateName.home, {
                url: '/home',
                templateUrl: 'home.main.html',
                controller: 'home.MainController'
            }).state(appConfig.stateName.recharge, {
                url: '/recharge?code&state',
                templateUrl: 'recharge.main.html',
                controller: 'recharge.MainController'
            }).state(appConfig.stateName.consumer, {
                url: '/consumer?code&state',
                templateUrl: 'consumer.main.html',
                controller: 'consumer.MainController'
            });

            RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json;charset=UTF-8'});
            RestangularProvider.setBaseUrl(appConfig.rootContext);

            /*
             RestangularProvider.setRequestInterceptor(function(elem, operation) {
             if (operation === "remove") {
             return undefined;
             }
             return elem;
             });
             */

             growlProvider.globalTimeToLive(5000);
             growlProvider.globalPosition('top-center');

        }
    ]).run([
        '$state',
        'Restangular',
        'alertService',
        'appConfig',
        function ($state, Restangular, alertService, appConfig) {

            Restangular.setErrorInterceptor(
                function(resp) {
                    if (resp && resp.status === 401 &&
                        resp.data && resp.data.path &&
                        (_.endsWith(resp.data.path, '/login') ||
                        _.endsWith(resp.data.path, 'users/auth'))) {
                        return true;
                    } else if (resp && resp.status === 401) {
                        alertService.warning('请先登录。');
                        $state.go(appConfig.stateName.login);
                    } else if (resp && resp.status === 403) {
                        alertService.error('只有管理员能使用这个功能，请用管理员账号登录。');
                    } else {
                        //alertService.error('无法访问光明订奶微信平台，请确认是否有网络！');
                    }
                    //loggerService.error(response);
                    return false; // stop the promise chain
                }
            );

        }
    ]);

})(window, window.angular);
