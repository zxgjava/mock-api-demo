package cn.agilean.wechat.controller.result;


public class DefaultResult implements ApplicationResult {
	private int result = 1;
	private String msg;
	
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public DefaultResult(int result, String msg) {
		super();
		this.result = result;
		this.msg = msg;
	}
	
	public DefaultResult() {
		super();
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

}
