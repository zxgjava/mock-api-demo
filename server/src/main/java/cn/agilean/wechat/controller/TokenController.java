package cn.agilean.wechat.controller;
 
import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.agilean.wechat.service.MockMpWeChatService;

import cn.agilean.weixin.domain.model.CheckModel;
import cn.agilean.weixin.domain.service.TokenService;
import cn.agilean.weixin.domain.utils.Tools;
 
@Controller
@RequestMapping("/mockmp")
public class TokenController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenController.class);
	@Autowired
    private TokenService tokenService;
	@Autowired
    private MockMpWeChatService weChatService;
     
	/**
     * 开发者模式token校验
     *
     * @param wxAccount 开发者url后缀
     * @param response
     * @param tokenModel
     * @throws ParseException
     * @throws IOException
     */
    @RequestMapping(value = "/msg", method = RequestMethod.GET, produces = "text/plain")
    public @ResponseBody String validate(@RequestParam(value = "signature", required = true) String signature,
    		@RequestParam(value = "timestamp", required = true) long timestamp,
    		@RequestParam(value = "nonce", required = true) long nonce,
    		@RequestParam(value = "echostr", required = true) String echostr
    		) throws ParseException, IOException {
    	CheckModel tokenModel = new CheckModel();
    	tokenModel.setSignature(signature);
    	tokenModel.setTimestamp(timestamp);
    	tokenModel.setNonce(nonce);
    	tokenModel.setEchostr(echostr);
    	String token = "weixin";
        return tokenService.validate(token,tokenModel);
    }
    
    @RequestMapping(value = "/msg", method = RequestMethod.POST, produces = "application/xml;charset=UTF-8")
    public @ResponseBody String postMsg(HttpServletRequest request, HttpServletResponse response) throws ParseException, IOException {
    	ServletInputStream in = request.getInputStream();
        String xmlMsg = Tools.inputStream2String(in);
        LOGGER.info("post-in:[" + xmlMsg + "]");
        return weChatService.processing(xmlMsg);
    }
    
    
}