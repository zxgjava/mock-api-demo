package cn.agilean.wechat.controller;


import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import cn.agilean.wechat.service.WxConsumerService;

import cn.agilean.weixin.domain.model.WxUserInfo;

/**
 * 
		* @ClassName: MockMpController 
		* @Description: TODO(这里用一句话描述这个类的作用) 
		* @author zxg 
		*
 */
@Controller
public class MockMpController {
	private static final Logger logger = LoggerFactory.getLogger(MockMpController.class);
	
	@Autowired
	private WxConsumerService wxConsumerService;

	@RequestMapping(value = "/mockmp", method = RequestMethod.GET)
    public ModelAndView mockmp() {
        return new ModelAndView("mockmp");
    }
	
	@RequestMapping(value = "/ok", method = RequestMethod.GET)
    public ModelAndView ok() {
        return new ModelAndView("ok");
    }
    
    @RequestMapping(value = "/mockmp/wxpay", method = RequestMethod.GET)
    public ModelAndView pay(HttpServletRequest request) {
    	String code = request.getParameter("code");
		String state = request.getParameter("state");
		logger.info("code:{}", code);
		logger.info("state:{}", state);
    	ModelAndView result = new ModelAndView("wxpay");
    	result.addObject("code", code);
    	result.addObject("state", state);
    	WxUserInfo userInfo = wxConsumerService.getConsumerWxInfo(code);
    	result.addObject("openId", userInfo.getOpenid());
    	result.addObject("nickName", userInfo.getNickname());
        return result;
    }
}
