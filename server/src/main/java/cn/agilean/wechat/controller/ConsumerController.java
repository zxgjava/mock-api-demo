package cn.agilean.wechat.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.agilean.wechat.service.MockMpConsumerService;
import cn.agilean.wechat.service.WxConsumerService;

import cn.agilean.redis.domain.model.ConsumerInfo;
import cn.agilean.redis.domain.model.ConsumerRecharge;
import cn.agilean.weixin.domain.model.WxUserInfo;

/**
 * 
		* @ClassName: ConsumerController 
		* @Description: TODO(这里用一句话描述这个类的作用) 
		* @author zxg 
		* @date 2015年10月15日 下午4:15:04 
		*
 */
@RestController
public class ConsumerController {
	private static final Logger logger = LoggerFactory.getLogger(ConsumerController.class);
	@Autowired
	private WxConsumerService wxConsumerService;
	@Autowired
	private MockMpConsumerService mockMpConsumerService;

	@RequestMapping(value="/mockmp/wxuser/{code}",method=RequestMethod.GET)
    public WxUserInfo showConsumerWxUser(@PathVariable String code)  {
    	logger.info("showConsumerWxUser-code:{}", code);
		return wxConsumerService.getConsumerWxInfo(code);
    }
    
    @RequestMapping(value="/mockmp/recharge", method = RequestMethod.GET)
    public List<ConsumerRecharge> getAllConsumerRecharge(@RequestParam(value = "openId", required = true) String openId) {
    	return mockMpConsumerService.getConsumerRechargeRepository().showConsumerRechargeList(openId);
    }
    
    @RequestMapping(value="/mockmp/money",method=RequestMethod.GET)
    public ConsumerInfo getConsumerInfo(@RequestParam(value = "openId", required = true) String openId){
    	return mockMpConsumerService.getConsumerInfoRepository().showConsumerInfo(openId);
    }
    
}
