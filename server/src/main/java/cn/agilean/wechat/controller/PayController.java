package cn.agilean.wechat.controller;

import cn.agilean.wechat.service.MockMpConstantsService;
import cn.agilean.wechat.service.MockMpConsumerService;
import cn.agilean.wechat.utils.WeChatTools;

import com.alibaba.fastjson.JSONObject;
import cn.agilean.weixin.domain.oauth.WechatPay;
import cn.agilean.weixin.domain.service.TokenService;
import cn.agilean.weixin.domain.service.WeChatService;
import cn.agilean.weixin.domain.utils.Tools;
import cn.agilean.weixin.domain.utils.XMLUtil;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

import org.apache.commons.lang.StringUtils;
import org.jdom2.JDOMException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.Future;

@Controller
@RequestMapping("/mockmp")
public class PayController {
    private static final Logger logger = LoggerFactory.getLogger(PayController.class);
    public static final String UTF_8 = "UTF-8";
    public static final String SUCCESS = "SUCCESS";

    @Autowired
    private MockMpConstantsService constantsService;
    @Autowired
    private WeChatService weChatService;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private WechatPay pay;
    @Autowired
    private MockMpConsumerService mockMpConsumerService;


	@RequestMapping(value = "/payinit", method = RequestMethod.POST)
    public
    @ResponseBody
    String payInit(HttpServletRequest request,
                   HttpServletResponse response) {
        String url = request.getParameter("url");
        // 判断是否微信环境, 5.0 之后的支持微信支付
        boolean isweixin = WeChatTools.isWeiXin(request);
        if (isweixin) {
            //config
            SortedMap<Object, Object> params = new TreeMap<Object, Object>();
            params.put("appId", MockMpConstantsService.MOCK_MP_APP_ID);
            String accessToken = tokenService.getAccessToken(MockMpConstantsService.MOCK_MP_APP_ID, MockMpConstantsService.MOCK_MP_APP_SECRET);
            String jsapiTicket = tokenService.getTicket(accessToken, MockMpConstantsService.MOCK_MP_APP_ID);
            Map<String, String> configMap = weChatService.jsApiSign(jsapiTicket, url);
            params.put("timestampConfig", configMap.get("timestamp"));
            params.put("noncestrConfig", configMap.get("noncestr"));
            params.put("signatureConfig", configMap.get("signature"));
            params.put("url", configMap.get("url"));
            logger.info("signatureConfig--->signatureConfig:{}", configMap.get("signature"));
            params.put("isweixin", "1");//微信版本号，用于前面提到的判断用户手机微信的版本是否是5.0以上版本。
            String json = JSONObject.toJSONString(params);
            return json;
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/pay", method = RequestMethod.GET)
    public
    @ResponseBody
    String postMsg(HttpServletRequest request,
                   HttpServletResponse response) {
        String openId = request.getParameter("openId");
        String total_fee = request.getParameter("money");
        logger.info("openId:{}", openId);
        logger.info("total_fee:{}", total_fee);
        // 判断是否微信环境, 5.0 之后的支持微信支付
        boolean isweixin = WeChatTools.isWeiXin(request);
        if (isweixin) {
            SortedMap<Object, Object> parameters = new TreeMap<Object, Object>();
            parameters.put("appid", MockMpConstantsService.MOCK_MP_APP_ID);
            parameters.put("mch_id", MockMpConstantsService.MOCK_MP_MCH_ID);
            parameters.put("nonce_str", Tools.createNonceStr());
            parameters.put("body", "MOCK-API充值");
            parameters.put("detail", "MOCK-API充值");
            String out_trade_no = Tools.getDateThroughSDF("yyyyMMddHHmmssSSS") + Tools.createTimestamp();
            parameters.put("out_trade_no", out_trade_no);
            parameters.put("total_fee", total_fee);
            parameters.put("spbill_create_ip", WeChatTools.getIp(request));
            parameters.put("notify_url", constantsService.getMockMpNotifyUrl());
            parameters.put("trade_type", "JSAPI");
            parameters.put("attach", "交易说明");
            parameters.put("openid", openId);
            String sign = pay.createSign(parameters, "UTF-8", MockMpConstantsService.MOCK_MP_API_KEY);
            parameters.put("sign", sign);

            String requestXML = Tools.getRequestXml(parameters);
            String respXML = pay.getUnifiedorderResult(requestXML);
            Map<String, String> map = new HashMap<String, String>();
            try {
                map = XMLUtil.doXMLParse(respXML);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String prepay_id = map.get("prepay_id");
            logger.info("getPrepayId--->prepay_id:{}", map.get("prepay_id"));

            SortedMap<Object, Object> params = new TreeMap<Object, Object>();
            params.put("appId", MockMpConstantsService.MOCK_MP_APP_ID);
            params.put("timeStamp", Tools.createTimestamp());
            params.put("nonceStr", Tools.createNonceStr());
            params.put("package", "prepay_id=" + prepay_id);
            params.put("signType", MockMpConstantsService.SIGN_TYPE);
            String paySign = pay.createSign(params, "UTF-8", MockMpConstantsService.MOCK_MP_API_KEY);
            params.put("packageValue", "prepay_id=" + prepay_id);    //这里用packageValue是预防package是关键字在js获取值出错
            params.put("paySign", paySign);     //paySign的生成规则和Sign的生成规则一致

            params.put("sendUrl", constantsService.getMockMpSuccessUrl()); //付款成功后跳转的页面
            params.put("isweixin", "1");//微信版本号，用于前面提到的判断用户手机微信的版本是否是5.0以上版本。
            String json = JSONObject.toJSONString(params);
            return json;
        } else {
            return null;
        }
    }
    
    @RequestMapping(value = "/mockPay", method = RequestMethod.GET)
    public
    @ResponseBody
    String mockPay(HttpServletRequest request, HttpServletResponse response) {
    	JSONObject result = new JSONObject();
    	result.put("msg", "FAIL");
        String openid = request.getParameter("openId");
        String total_fee = request.getParameter("totalFee");
        String appid = request.getParameter("appId");
        JSONObject json = new JSONObject();
        json.put("appid", appid);
        json.put("openid", openid);
        json.put("total_fee", total_fee);
        logger.info("mockpay--jsonBody:{};", json.toJSONString());
        try {
	        com.ning.http.client.Request requestPlus = new RequestBuilder("POST").setUrl("http://mock-api.com/FPfXxIeuuSQV0oJq.mock/rest/pay/wechat")
					.addHeader("Content-Type", "application/json;charset=utf-8")
					.setBody(json.toJSONString()).build();
	        AsyncHttpClient httpClient = new AsyncHttpClient();
			Future<Response> f = httpClient.executeRequest(requestPlus);
			if (f.get().getResponseBody().indexOf("SUCCESS") > -1) {
				result.put("msg", "SUCCESS");
			}
			logger.info("mockpay--f.get().getResponseBody():{};", f.get().getResponseBody());
			httpClient.close();
			return result.toJSONString();
		} catch (Exception e) {
			e.printStackTrace();
			return result.toJSONString();
		}
    }

    @RequestMapping(value = "/recharge/notify", method = RequestMethod.POST)
    public @ResponseBody String rechargeNotify(@RequestBody String notifyXml) {
        try {
            logger.info("weixin notify msg is {}", notifyXml);
            Map<String, String> notify = XMLUtil.doXMLParse(notifyXml);
            SortedMap<Object, Object> treeMap = new TreeMap<>();
            treeMap.putAll(notify);
            String mySign = pay.createSign(treeMap, UTF_8, MockMpConstantsService.MOCK_MP_API_KEY);
            String sign = notify.get("sign");
//            if (StringUtils.equals(mySign, sign)) {
            if (true) {
                String resultCode = notify.get("result_code");
                if (StringUtils.equals(resultCode, SUCCESS)) {
                	mockMpConsumerService.receiveNotify(notify, MockMpConstantsService.MOCK_MP_APP_ID, MockMpConstantsService.MOCK_MP_APP_SECRET);
                    SortedMap<Object, Object> resultMap = new TreeMap<>();
                    resultMap.put("return_code", SUCCESS);
                    resultMap.put("return_msg", "OK");
                    return Tools.getRequestXml(resultMap);
                } else {
                    logger.error("weixin pay callback has error,err_code:{},err_code_des:{}", notify.get("err_code"), notify.get("err_code_des"));
                }
            } else {
                SortedMap<Object, Object> resultMap = new TreeMap<>();
                resultMap.put("return_code", "FAIL");
                resultMap.put("return_msg", "签名失败");
                return Tools.getRequestXml(resultMap);
            }
        } catch (JDOMException e) {
            logger.error("parse weixin pay success notify fail! xml is {}", notifyXml);
        } catch (IOException e) {
            logger.error("parse weixin pay success notify fail! xml is {}", notifyXml);
        }
        return null;
    }

}
