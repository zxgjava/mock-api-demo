package cn.agilean.wechat.service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.agilean.redis.domain.model.ConsumerRecharge;
import cn.agilean.redis.domain.repository.ConsumerInfoRepository;
import cn.agilean.redis.domain.repository.ConsumerRechargeRepository;
import cn.agilean.weixin.domain.oauth.Message;
import cn.agilean.weixin.domain.service.TokenService;


@Service
public class MockMpConsumerService {
	private static final Logger logger = LoggerFactory.getLogger(MockMpConsumerService.class);
    private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    private static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    @Autowired
    private TokenService tokenService;

    @Autowired
    private Message message;
	
	@Autowired
	private ConsumerInfoRepository consumerInfoRepository;
	@Autowired
	private ConsumerRechargeRepository consumerRechargeRepository;
	public ConsumerInfoRepository getConsumerInfoRepository() {
		return consumerInfoRepository;
	}
	public ConsumerRechargeRepository getConsumerRechargeRepository() {
		return consumerRechargeRepository;
	}
	
	private Boolean recharge (ConsumerRecharge consumerRecharge) {
		ConsumerRecharge recharge = consumerRechargeRepository.showConsumerRecharge(consumerRecharge.getTransactionId());
		if (recharge != null) {
			return false;
		}
		consumerInfoRepository.updateConsumerInfo(consumerRecharge);
		consumerRechargeRepository.saveConsumerRecharge(consumerRecharge);
		return true;
	}
	
	public void receiveNotify(Map<String, String> notify, String appId, String appSecret) {
        Boolean success = recharge(createConsumerRecharge(notify));
        if (success) {
            String accessToken = tokenService.getAccessToken(appId, appSecret);
            String openid = notify.get("openid");
            BigDecimal fen = new BigDecimal(notify.get("total_fee"));
            BigDecimal yuan = fen.divide(new BigDecimal(100));
            LocalDateTime localDateTime = LocalDateTime.parse(notify.get("time_end"), DateTimeFormatter.ofPattern(YYYYMMDDHHMMSS));
            String rechargeTime = localDateTime.format(DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MM_SS));
            Double amount = yuan.doubleValue();
            String notifyText = String.format("您在%s成功充值%.2f元!", rechargeTime, amount);
            message.sendText(accessToken, openid, notifyText);
        }
    }

    private ConsumerRecharge createConsumerRecharge(Map<String, String> notify) {
        ConsumerRecharge consumerRecharge = new ConsumerRecharge();
        consumerRecharge.setOpenId(notify.get("openid"));
        consumerRecharge.setTimeEnd(notify.get("time_end"));
        consumerRecharge.setMchId(notify.get("mch_id"));
        consumerRecharge.setBankType(notify.get("bank_type"));
        consumerRecharge.setAppId(notify.get("appid"));
        consumerRecharge.setRechargeAmount(Integer.valueOf(notify.get("total_fee")));
        consumerRecharge.setTradeType(notify.get("trade_type"));
        consumerRecharge.setTransactionId(notify.get("transaction_id"));
        return consumerRecharge;
    }
}
