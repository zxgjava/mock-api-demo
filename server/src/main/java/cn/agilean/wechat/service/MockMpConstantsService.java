package cn.agilean.wechat.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MockMpConstantsService {
	
	//mock-api mp
	public static final String MOCK_MP_APP_ID = "wx22ddb73aa5b2707f";
	public static final String MOCK_MP_APP_SECRET = "728761d9d39b9fc693214b0a1c1b6dec";
	public static final String MOCK_MP_MCH_ID = "1321576901";
	public static final String MOCK_MP_API_KEY = "8648e7928f01f291189241c73f2b0904";
	public static final String MOCK_MP_WECHAT_CODE = "mockapi";//微信号
	public static final String MOCK_MP_WECHAT_ID = "gh_5a7c45ae4ddf";//原始ID
	
	public static final String ACCESSTOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
	public static final String JSAPI_TICKET = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=";
	public static final String UNIFIEDORDER_URI = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	public static final String SIGN_TYPE = "MD5";
	public static final String MOCK_MP_EVENT_KEY_V001_HUODONG = "V001_HUODONG";
	public static final String MOCK_MP_SUBSCRIBE_TEXT = "欢迎您关注Mock Api微信平台";
	public static final String MOCK_MP_SCAN_TEXT = "欢迎您使用Mock Api微信平台";

	@Value("${publisher.callback.hostname}")
	private String publisherCallBackHostName;
	
	//授权回调地址
	private String mockMpRedirectUri = "/mockmp/wxpay";
	
	
	
	private String mockMpNotifyUrl = "/mockmp/recharge/notify";
	private String mockMpPayUrl = "/mockmp/wxpay";
	private String mockMpSuccessUrl = "/mockmp";
	
	public String getMockMpRedirectUri() {
		return publisherCallBackHostName+mockMpRedirectUri;
	}
	public String getMockMpSuccessUrl() {
		return publisherCallBackHostName+mockMpSuccessUrl;
	}
	public String getMockMpNotifyUrl() {
		return publisherCallBackHostName+mockMpNotifyUrl;
	}
	
	public String getMockMpPayUrl() {
		return publisherCallBackHostName+mockMpPayUrl;
	}
		
}