package cn.agilean.wechat.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import cn.agilean.weixin.domain.model.WxUserInfo;
import cn.agilean.weixin.domain.oauth.Oauth;
import cn.agilean.weixin.domain.oauth.User;
import cn.agilean.weixin.domain.service.TokenService;

@Service
public class WxConsumerService {
	private static final Logger logger = LoggerFactory.getLogger(WxConsumerService.class);
	/**
	 * 微信授权接口
	 */
	@Autowired
	private Oauth oauth;
	/**
	 * 微信用户接口
	 */
	@Autowired
	private User user;
	@Autowired
	private TokenService tokenService;
	
	public WxUserInfo getConsumerWxInfo(String code){
		String oauthTokenJsonStr = oauth.getToken(MockMpConstantsService.MOCK_MP_APP_ID, MockMpConstantsService.MOCK_MP_APP_SECRET, code);
		logger.info("oauthTokenJsonStr:{}", oauthTokenJsonStr);
		Map<String, Object> map = JSONObject.parseObject(oauthTokenJsonStr);
		String openId = map.get("openid").toString();
		String accessToken = tokenService.getAccessToken(MockMpConstantsService.MOCK_MP_APP_ID, MockMpConstantsService.MOCK_MP_APP_SECRET);
		return user.getUserInfo(accessToken, openId);
	}
	
	public String getConsumerOpenId(String code){
		String oauthTokenJsonStr = oauth.getToken(MockMpConstantsService.MOCK_MP_APP_ID, MockMpConstantsService.MOCK_MP_APP_SECRET, code);
		logger.info("oauthTokenJsonStr:{}", oauthTokenJsonStr);
		Map<String, Object> map = JSONObject.parseObject(oauthTokenJsonStr);
		String openId = map.get("openid").toString();
		return openId;
	}
}
