package cn.agilean.wechat.service;

import cn.agilean.redis.domain.model.ConsumerInfo;
import cn.agilean.weixin.domain.model.Articles;
import cn.agilean.weixin.domain.model.InMessage;
import cn.agilean.weixin.domain.model.OutMessage;
import cn.agilean.weixin.domain.model.WxUserInfo;
import cn.agilean.weixin.domain.oauth.Message;
import cn.agilean.weixin.domain.oauth.User;
import cn.agilean.weixin.domain.service.MessageProcessingHandler;
import cn.agilean.weixin.domain.service.TokenService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class MockMpMessageProcessingHandlerImpl implements MessageProcessingHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockMpMessageProcessingHandlerImpl.class);

    private OutMessage outMessage;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private User user;
    @Autowired
    private MockMpConsumerService mockMpConsumerService;

    /**
     * 消息操作接口
     */
    @Autowired
    private Message message;

    @Override
    public void allType(InMessage msg) {
    	LOGGER.info("allType:---{}--", msg.getEvent());
//        TextOutMessage out = new TextOutMessage();
//        out.setContent("您的消息已经收到！");
//        setOutMessage(out);
    }

    @Override
    public void textTypeMsg(InMessage msg) {
    }

    @Override
    public void locationTypeMsg(InMessage msg) {
    }

    @Override
    public void imageTypeMsg(InMessage msg) {
    }

    @Override
    public void videoTypeMsg(InMessage msg) {
    }

    @Override
    public void voiceTypeMsg(InMessage msg) {
    }

    @Override
    public void linkTypeMsg(InMessage msg) {
    }

    @Override
    public void verifyTypeMsg(InMessage msg) {
    }

    @Override
    public void eventTypeMsg(InMessage msg){
    	String accessToken = tokenService.getAccessToken(MockMpConstantsService.MOCK_MP_APP_ID, MockMpConstantsService.MOCK_MP_APP_SECRET);
        LOGGER.info("accessToken:---{}--", accessToken);
        LOGGER.info("msg.getEvent():---{}--", msg.getEvent());
        LOGGER.info("msg.getEventKey():---{}--", msg.getEventKey());
        String openId = msg.getFromUserName();
        
    	switch (msg.getEvent()) {
    		case "CLICK":
    			if (msg.getEventKey().equals(MockMpConstantsService.MOCK_MP_EVENT_KEY_V001_HUODONG)) {
    	    		List<Articles> articles = new ArrayList<Articles>();
    	            for (int i = 0; i < 4; i++) {
    	                Articles a = new Articles();
    	                a.setTitle("agilean咨询团队" + (i + 1));
    	                a.setDescription("agilean咨询团队" + (i + 1));
    	                a.setPicurl("http://b.hiphotos.baidu.com/image/w%3D230/sign=4f00d1220e3387449cc5287f610fd937/30adcbef76094b36158b949fa1cc7cd98d109da3.jpg");
    	                a.setUrl("http://www.agilean.cn/");
    	                articles.add(a);
    	            }
    	            message.SendNews(accessToken, openId, articles);
    	    	}
    			break;
    		case "subscribe":
    			/**
    			 * 扫描带参数二维码，关注公众号绑定卡
    			 * 1》.未绑卡关注
    			 * 2》.已绑卡关注（）
    			 * qrscene_{cardId}
    			 */
    			actionScan(msg, accessToken);
    			break;
    		case "SCAN":
    			/**
    			 * 1、（之前已关注）扫描带参数二维码，关注公众号未绑定卡
    			 * 2、（之前已关注）扫描带参数二维码，关注公众号已绑定卡
    			 */
    			actionScan(msg, accessToken);
    			break;
    		default :
    			LOGGER.info("msg.getEvent():---{}--default", msg.getEvent());
    	}
    }

    @Override
    public void setOutMessage(OutMessage outMessage) {
        this.outMessage = outMessage;
    }

    @Override
    public void afterProcess(InMessage inMessage, OutMessage outMessage) {
    }

    @Override
    public OutMessage getOutMessage() {
        return outMessage;
    }
    /**
     * 针对关注和扫描的action 
		* @param msg
		* @param accessToken
     */
    private void actionScan(InMessage msg, String accessToken){
        String openId = msg.getFromUserName();
        WxUserInfo wxUserInfo = user.getUserInfo(accessToken, openId);
        wxUserInfo.setPublisherId(MockMpConstantsService.MOCK_MP_APP_ID);
        ConsumerInfo consumerInfo = new ConsumerInfo();
		consumerInfo.setOpenId(openId);
		consumerInfo.setUnionId("unionId");
		consumerInfo.setTotalMoneyAmount(0);
    	switch (msg.getEvent()) {
			case "subscribe":
				String text = MockMpConstantsService.MOCK_MP_SUBSCRIBE_TEXT;
    			message.sendText(accessToken, msg.getFromUserName(), text);
    			mockMpConsumerService.getConsumerInfoRepository().saveConsumerInfo(consumerInfo );
    			LOGGER.info("subscribe----end...");
				break;
			case "SCAN":
				String scanText = MockMpConstantsService.MOCK_MP_SCAN_TEXT;
    			message.sendText(accessToken, msg.getFromUserName(), scanText);
    			mockMpConsumerService.getConsumerInfoRepository().saveConsumerInfo(consumerInfo );
    			LOGGER.info("SCAN----end...");
				break;
			default :
				LOGGER.info("msg.getEvent():---{}--default", msg.getEvent());
    	}
    }
    
}
